package com.servConsultorias.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "SERVICIO")
public class Servicio {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Servicio")
	private long id;
	
	
	@Column(nullable = false)
	private String nombre;
	
	
	@Column(nullable = false)
	private String imagen;
	
	
	@Column(nullable = false)
	private String descripcion;
	
	
	@Column(nullable = false)
	private String categoria;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="servicio")
	@JsonIgnore
	private Set<Cotizacion> cotizaciones = new HashSet<Cotizacion>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "consultor_servicio", nullable = false)
	@JsonIgnore
	private Consultor consultor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Set<Cotizacion> getCotizaciones() {
		return cotizaciones;
	}

	public void setCotizaciones(Set<Cotizacion> cotizaciones) {
		this.cotizaciones = cotizaciones;
	}

	public Consultor getConsultor() {
		return consultor;
	}

	public void setConsultor(Consultor consultor) {
		this.consultor = consultor;
	}
	
	
	
}
