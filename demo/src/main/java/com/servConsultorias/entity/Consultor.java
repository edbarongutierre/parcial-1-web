package com.servConsultorias.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "CONSULTOR")
public class Consultor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_consultor")
	private long id;
	
	
	@Column(nullable = false)
	private String nombre;
	
//	@OneToMany(fetch=FetchType.LAZY, mappedBy="consultor")
//	@JsonIgnore
//	private Set<Cotizacion> cotizacion = new HashSet<Cotizacion>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="consultor")
	@JsonIgnore
	private Set<Servicio> servicios = new HashSet<Servicio>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

//	public Set<Cotizacion> getCotizacion() {
//		return cotizacion;
//	}
//
//	public void setCotizacion(Set<Cotizacion> cotizacion) {
//		this.cotizacion = cotizacion;
//	}

	public Set<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(Set<Servicio> servicios) {
		this.servicios = servicios;
	}
	
	
	
}
