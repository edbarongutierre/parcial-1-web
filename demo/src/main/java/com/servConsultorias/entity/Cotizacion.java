package com.servConsultorias.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "COTIZACION")
public class Cotizacion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Cotizacion")
	private long id;
	
	
	@Column(nullable = false)
	private String descripcion;
	
	
	@Column(nullable = false)
	private int valor;
	
	
	@Column(nullable = false)
	private Date fecha;
	
	
	@ManyToOne(fetch =  FetchType.LAZY)
	@JsonIgnore
	private Cliente cliente;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicio_cotizacion", nullable = false)
	@JsonIgnore
	private Servicio servicio;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "consultor_cotizacion", nullable = false)
//	private Consultor consultor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

//	public Consultor getConsultor() {
//		return consultor;
//	}
//
//	public void setConsultor(Consultor consultor) {
//		this.consultor = consultor;
//	}
	
	
	
}
