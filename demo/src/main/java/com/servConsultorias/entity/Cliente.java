package com.servConsultorias.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "CLIENTE")
public class Cliente {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_Cliente")
	private long id;
	
	
	@Column(nullable = false)
	private String nombre;	

	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
	@JsonIgnore
	private Set<Cotizacion> cotizaciones = new HashSet<Cotizacion>();


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Set<Cotizacion> getCotizaciones() {
		return cotizaciones;
	}


	public void setCotizaciones(Set<Cotizacion> cotizaciones) {
		this.cotizaciones = cotizaciones;
	}
	
	
	
}


