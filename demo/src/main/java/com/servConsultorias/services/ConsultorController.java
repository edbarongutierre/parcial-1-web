package com.servConsultorias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.servConsultorias.entity.Consultor;
import com.servConsultorias.repository.ConsultorRepository;

@RestController
public class ConsultorController {
	
	@Autowired
	private ConsultorRepository consultorRepositoryDAO;
	
	@RequestMapping("/getAllConsultores")
	public Iterable<Consultor> getAllConsultores(){
		Iterable<Consultor> findAll = consultorRepositoryDAO.findAll();
		return findAll;	
	}
	
	@RequestMapping(path="/addConsultor", method=RequestMethod.GET) 
	public @ResponseBody String addNewCliente
	(@RequestParam String nombre) {
		
		Consultor consultor = new Consultor();
		consultor.setNombre(nombre);
		consultorRepositoryDAO.save(consultor);
		return "Consultor Guardado";	
	}
}
