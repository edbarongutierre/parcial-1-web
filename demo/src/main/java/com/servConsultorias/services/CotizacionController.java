package com.servConsultorias.services;

import java.sql.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.servConsultorias.entity.Cliente;
import com.servConsultorias.entity.Consultor;
import com.servConsultorias.entity.Cotizacion;
import com.servConsultorias.entity.Servicio;
import com.servConsultorias.repository.ClienteRepository;
import com.servConsultorias.repository.ConsultorRepository;
import com.servConsultorias.repository.CotizacionRepository;
import com.servConsultorias.repository.ServicioRepository;

@RestController
public class CotizacionController {
	
	@Autowired
	private CotizacionRepository cotizacionRepositoryDAO;
	
	@Autowired
	private ClienteRepository clienteRepositoryDAO;
	
	@Autowired
	private ConsultorRepository consultorRepositoryDAO;
	
	@Autowired
	private ServicioRepository servicioRepositoryDAO;

	
	
	@RequestMapping("/getAllCotizaciones")
	public Iterable<Cotizacion> getAllCotizaciones(){
		Iterable<Cotizacion> findAll = cotizacionRepositoryDAO.findAll();
		return findAll;	
	}
	
	
	@RequestMapping(path="/addCotizacion", method=RequestMethod.GET) 
	public @ResponseBody String addNewCotizacion(
			@RequestParam String descripcion, 
			@RequestParam int valor, 
			@RequestParam Long idCliente,
			@RequestParam Long idServicio
//			@RequestParam Long idConsultor
			) {
		
		Optional<Cliente> cliente = clienteRepositoryDAO.findById(idCliente);
		if(!cliente.isPresent()) return "El id del Cliente no existe!";
		
//		Optional<Consultor> consultor = consultorRepositoryDAO.findById(idConsultor);
//		if(!consultor.isPresent()) return "El id del Consultor no existe!";
		
		Optional<Servicio> servicio = servicioRepositoryDAO.findById(idServicio);
		if(!servicio.isPresent()) return "El id del Servicio no existe!";
		
		
		Cliente clienteSi = cliente.get();
		Servicio servicioSi = servicio.get();
		
		
		Cotizacion cotizacion = new Cotizacion();
		cotizacion.setDescripcion(descripcion);
		cotizacion.setValor(valor);
		cotizacion.setFecha(new Date(System.currentTimeMillis()));
		cotizacion.setCliente(clienteSi);
		cotizacion.setServicio(servicioSi);
		cotizacionRepositoryDAO.save(cotizacion);
		return "Cotizacion Guardada";	
	}
	
	@RequestMapping(path="/findCotizacionPorServicio", method=RequestMethod.GET) 
	public @ResponseBody Iterable<Cotizacion> cotizacionPorServicio (		
			@RequestParam Long idServicio) {
		
		Optional<Servicio> servicio = servicioRepositoryDAO.findById(idServicio);
		if(!servicio.isPresent()) return null;
		
		Servicio servicioSi = servicio.get();
		
		Iterable<Cotizacion> findAll = cotizacionRepositoryDAO.findByServicio(servicioSi);
		return findAll;	
		
	}
	
//	@RequestMapping(path="/findCotizacionPorConsultor", method=RequestMethod.GET) 
//	public @ResponseBody Iterable<Cotizacion> cotizacionPorConsultor (		
//			@RequestParam Long idConsultor) {
//		
//		Optional<Consultor> consultor = consultorRepositoryDAO.findById(idConsultor);
//		if(!consultor.isPresent()) return null;
//		
//		Consultor consultorSi = consultor.get();
//		
//		Iterable<Cotizacion> findAll = cotizacionRepositoryDAO.findByConsultor(consultorSi);
//		return findAll;	
//	}	
	
}