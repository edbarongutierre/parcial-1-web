package com.servConsultorias.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.servConsultorias.entity.Cliente;
import com.servConsultorias.repository.ClienteRepository;

@RestController
public class ClienteController {
	
	@Autowired
	private ClienteRepository clienteRepositoryDAO;
	
	@RequestMapping("/getAllClientes")
	public Iterable<Cliente> getAllUsers () {
		Iterable<Cliente> findAll = clienteRepositoryDAO.findAll();
		return findAll;	
	}
	
	@RequestMapping(path="/addCliente", method=RequestMethod.GET) 
	public @ResponseBody String addNewCliente
	(@RequestParam String nombre) {
		
		Cliente cliente = new Cliente();
		cliente.setNombre(nombre);
		clienteRepositoryDAO.save(cliente);
		return "Cliente Guardado";
		
	}
}
