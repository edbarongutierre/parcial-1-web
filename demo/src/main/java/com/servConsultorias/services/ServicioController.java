package com.servConsultorias.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.servConsultorias.entity.Consultor;
import com.servConsultorias.entity.Servicio;
import com.servConsultorias.repository.ConsultorRepository;
import com.servConsultorias.repository.ServicioRepository;

@RestController
public class ServicioController {
	
	@Autowired
	private ServicioRepository servicioRepositoryDAO;
	

	@Autowired
	private ConsultorRepository consultorRepositoryDAO;
	
	@RequestMapping("/getAllServicios")
	public Iterable<Servicio> getAllServicios(){
		Iterable<Servicio> findAll = servicioRepositoryDAO.findAll();
		return findAll;	
	}
	
	@RequestMapping(path="/addServicio", method=RequestMethod.GET) 
	public @ResponseBody String addNewServicio(
			@RequestParam String nombre, 
			@RequestParam String imagen, 
			@RequestParam String descripcion,
			@RequestParam String categoria,
			@RequestParam Long idConsultor) {
		
		
		Optional<Consultor> consultor = consultorRepositoryDAO.findById(idConsultor);
		if(!consultor.isPresent()) return "El id del Consultor no existe!";
	
		Consultor consultorSi = consultor.get();
	
		Servicio servicio = new Servicio();
		servicio.setNombre(nombre);
		servicio.setImagen(imagen);
		servicio.setDescripcion(descripcion);
		servicio.setCategoria(categoria);
		servicio.setConsultor(consultorSi);
		
		servicioRepositoryDAO.save(servicio);
		return "Servicio Guardado";	
	}
	
	@RequestMapping(path="/findServiciosPorConsultor", method=RequestMethod.GET) 
	public @ResponseBody Iterable<Servicio> serviciosPorConsultor (		
			@RequestParam Long idConsultor) {
		
		Optional<Consultor> consultor = consultorRepositoryDAO.findById(idConsultor);
		if(!consultor.isPresent()) return null;
		
		Consultor consultorSi = consultor.get();
		
		Iterable<Servicio> findAll = servicioRepositoryDAO.findByConsultor(consultorSi);
		return findAll;	
		
	}
}
