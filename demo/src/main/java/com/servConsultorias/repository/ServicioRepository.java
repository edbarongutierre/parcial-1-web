package com.servConsultorias.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.servConsultorias.entity.Consultor;
import com.servConsultorias.entity.Servicio;

@Repository
public interface ServicioRepository extends CrudRepository<Servicio, Long>{

	Iterable<Servicio> findByConsultor(Consultor consultorSi);
	
}
