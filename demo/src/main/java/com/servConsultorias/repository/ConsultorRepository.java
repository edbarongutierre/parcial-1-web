package com.servConsultorias.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.servConsultorias.entity.Consultor;

@Repository
public interface ConsultorRepository extends CrudRepository<Consultor, Long>{
	
}
