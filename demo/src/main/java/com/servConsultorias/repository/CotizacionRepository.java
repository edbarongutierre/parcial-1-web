package com.servConsultorias.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.servConsultorias.entity.Cotizacion;
import com.servConsultorias.entity.Servicio;

@Repository
public interface CotizacionRepository extends CrudRepository<Cotizacion, Long>{

	public Iterable<Cotizacion> findByServicio(Servicio servicioSi);
	
}
